import * as logger from "firebase-functions/logger";
import {onObjectFinalized} from "firebase-functions/v2/storage";
import axios from "axios";

export const processImage = onObjectFinalized(async (event) => {
  const filePath = event.data.name;
  try {
    const results = await axios({
      method: "POST",
      url: "https://detect.roboflow.com/wildfire_detection-slvoz/1",
      params: {
        api_key: "GeALLnJG9aiEqA6Ej9hZ",
        image: filePath,
      },
    });
    logger.log(results);
  } catch (error) {
    logger.error(error);
  }
});

