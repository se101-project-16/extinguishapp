import { initializeApp } from 'firebase/app';
import {getAuth} from 'firebase/auth';
import {getStorage} from 'firebase/storage';

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyBYheTW8vXCQXe5g8mIu-XL9rXIjbbv-Vo",
    authDomain: "extinguish-cfb83.firebaseapp.com",
    projectId: "extinguish-cfb83",
    storageBucket: "extinguish-cfb83.appspot.com",
    messagingSenderId: "444221387920",
    appId: "1:444221387920:web:03b5d36e5036bfb6b19415",
    measurementId: "G-3NY0XHVKBG"
  };

export const FIREBASE_APP = initializeApp(firebaseConfig);
export const FIREBASE_AUTH = getAuth(FIREBASE_APP);
export const FIREBASE_STORAGE =getStorage(FIREBASE_APP);
